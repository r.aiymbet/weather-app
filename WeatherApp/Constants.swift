import Foundation

struct Constants {
    static let host = "https://api.openweathermap.org/data/2.5/onecall"
    static var lat = "40.74"
    static var long = "-73.99"
    static let exclude = "alerts,minutely"
    static let apiKey = "fe35fb943e961ed24d480c75b8076cdf"
}
